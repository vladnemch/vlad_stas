var
  n: integer;
  ch: Char;

begin
  write('n= ');
  readln(n);
  if (n >= 0) and (n <= 15) then
  begin
    if n < 10 then
      ch := chr(ord('0') + n)
    else ch := chr(ord('A') + n - 10);
    writeln('n= ', ch)
  end
  else
    writeln('Error')
end.