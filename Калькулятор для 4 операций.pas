var
  operation: char;
  x, y, z: real;
  stop: boolean;

begin
  stop := False;
  repeat
    writeln;
    write('x,y= ');
    readln(x, y);
    write('операция: ');
    readln(operation);
    case operation of 
      '+': z := x + y;
      '-': z := x - y;
      '*': z := x * y;
      '/': z := x / y;
    else
      stop := true;
    end;
    if not stop then
      writeln(' результат= ', z)
  until stop
end.